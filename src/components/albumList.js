import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Tooltip from '@mui/material/Tooltip';

const useStyles = makeStyles({
    root: {
        padding: '2vw',
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        overflow: 'hidden',
    },
    itemWrapper: {},
    toggleButtonsWrapper: {
        width: '100%',
        textAlign: 'center',
        marginBottom: 20,
    },
    albumsWrapper: {
      overflow: 'auto',
      flexGrow: 1,
      padding: 10,
    },
    cardRoot: {
        cursor: 'pointer',
        outline: '#CBF1F5 solid 5px',
        backgroundColor: '#A6E3E9',
        transition: 'all 0.5s !important',
        '&:hover': {
            outline: '#71C9CE solid 5px',
        }
    },
    coverImageWrapper: {
        position: 'relative',
    },
    qobuzLogo: {
        position: 'absolute',
        bottom: 8,
        right: 8,
        width: 'clamp(10px,10%,20px)',
    },
    title: {
        whiteSpace: 'nowrap',
        width: '100%',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
    },
    noDataView: {
        height: '100%',
        display: 'grid',
        placeContent: 'center',

    },
    togglegroup: {
        '& .MuiToggleButton-root': {
            backgroundColor: '#CBF1F5',
            fontWeight: 'bold',
        },
        '& .Mui-selected': {
            color: 'unset !important',
            backgroundColor: '#E3FDFD !important'
        },
    }
});



const AlbumList = ({songList}) => {
    const classes = useStyles();

    const [source, setSource] = React.useState('ALL');

    const handleChange = (event, value) => {
        setSource(value);
      };

    const AlbumItem = (data) => {
        return (
        <Card className={classes.cardRoot}>
            <div className={classes.coverImageWrapper}>
                <CardMedia
                    component="img"
                    image={ data.cover ? `/images/covers/${data.cover}` : '/images/undefined_album_cover.png'}
                    alt={`${data.album} cover`}
                />
                {data.source === 'QOBUZ' && <img className={classes.qobuzLogo} src="/images/qobuz.png" alt="qobuz-logo" />}
            </div>
            <CardContent>
                <Tooltip title={data.album}>
                    <Typography gutterBottom variant="subtitle1" component="div" className={classes.title}>
                    {data.album}
                    </Typography>
                </Tooltip>
                <Typography variant="caption" color="text.secondary">
                {data.artist}
                </Typography>
            </CardContent>
        </Card>
        )
    }

    return (
        <div className={classes.root}>
            <div className={classes.toggleButtonsWrapper}>
                <ToggleButtonGroup
                    color="primary"
                    value={source}
                    exclusive
                    onChange={handleChange}
                    className={classes.togglegroup}
                    >
                    <ToggleButton value="ALL">All</ToggleButton>
                    <ToggleButton value="LOCAL">Local</ToggleButton>
                    <ToggleButton value="QOBUZ">QOBUZ</ToggleButton>
                </ToggleButtonGroup>
            </div>
            <div className={classes.albumsWrapper}>
                <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12, lg: 24 }}>
                    {songList.map((data, index) => (
                        (source === data.source || source === 'ALL') ? 
                        <Grid item xs={2} sm={4} md={4} key={index}>
                            {AlbumItem(data)}
                        </Grid>: null
                    ))}
                </Grid>
            </div>
        </div>
    )
}

const mapStateToProps = ({songList}) => {
    return {
      songList
    };
  }

export default connect(mapStateToProps)(AlbumList);