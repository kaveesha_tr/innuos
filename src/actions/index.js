import { GET_SONGS } from "./types";

export const getSongs = () => {
    return (dispatch, getState) => {
        fetch('albums.json'
            ,{
              headers : { 
                'Content-Type': 'application/json',
                'Accept': 'application/json'
               }
            })
              .then(function(response){
                return response.json();
              })
              .then(function(albums) {
                dispatch ({ type: GET_SONGS, payload: albums})
              });
    }
}