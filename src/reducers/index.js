import { combineReducers } from 'redux';
import { GET_SONGS } from '../actions/types';


const getSongsReducer = (payload = null, action) => {
    if (action.type === GET_SONGS) {
        return action.payload;
    }
    return payload;
}

export default combineReducers({
    songList: getSongsReducer
});