import React from 'react';
import { makeStyles } from '@mui/styles';
import { connect } from 'react-redux';
import { getSongs } from './actions';
import Button from '@mui/material/Button';
import AlbumList from './components/albumList';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MusicNoteRoundedIcon from '@mui/icons-material/MusicNoteRounded';

const useStyles = makeStyles({
  root: {
    backgroundColor: '#E3FDFD',
    height: '100vh',
  },
  content: {
      // padding: '2vh 2vw',
      height: '100vh',
      display: 'flex',
      flexDirection: 'column',
      maxWidth: 1500,
      margin: '0 auto',
  },
  button: {
    marginTop: '2vh !important',
    backgroundColor: '#71C9CE !important',
  },
  noDataView: {
    height: '100%',
    display: 'grid',
    placeContent: 'center',
  },
  roundButton: {
    backgroundColor: '#A6E3E9 !important',
    width: '13em',
    height: '13em',
    flexDirection: 'column',
    outline: '#71C9CE solid 0 !important',
    transition: 'all 0.5s !important',
    '& .MuiSvgIcon-root': {
      height: '8em',
      width: '8em',
      color: '#fff',
      transition: 'all 0.5s !important',
    },
    '&:hover': {
      outline: '#71C9CE solid 1em !important',
      '& .MuiSvgIcon-root': {
        transform:' rotate(15deg)',
      },
    },
  },
  buttonText: {
    fontWeight: 'bolder  !important',
    color: '#fff',
  }
});

const App = ({getSongs, songList}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {songList? 
        <div className={classes.content}>
          
          <Button variant="contained" onClick={getSongs} className={classes.button}>{!songList ? 'GET ALBUMS' : 'REFRESH ALBUMS'}</Button>
          <AlbumList />
          
      </div>
      :
      <div className={classes.noDataView}>
          <IconButton component="button" className={classes.roundButton} onClick={getSongs}>
            <MusicNoteRoundedIcon />
            <Typography variant="h6" className={classes.buttonText}>GET ALBUMS</Typography>
          </IconButton>
          {/* <Typography variant="h4" color="#71C9CE">Plaese click "GET ALBUMS" to start</Typography> */}
      </div>
      }
    </div>
  );
}

const mapStateToProps = ({songList}) => {
  return {
    songList
  };
}

export default connect(mapStateToProps, {getSongs})(App);
